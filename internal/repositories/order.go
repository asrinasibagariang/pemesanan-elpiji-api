// Package repositories
package repositories

import (
	"context"
	"database/sql"

	"gitlab.privy.id/go_graphql/internal/entity"
	"gitlab.privy.id/go_graphql/pkg/postgres"
)

type Order interface {
	Get(context.Context) ([]entity.Order, error)
	UpsertOrder(context.Context, entity.Order) (uint64, error)
	GetById(ctx context.Context, id int64) (entity.Order, error)
}

type orderImplementation struct {
	conn postgres.Adapter
}

func (r orderImplementation) Get(ctx context.Context) ([]entity.Order, error) {
	query := `
		SELECT  
		id,
		jumlah_order,
		id_pangkalan,
		id_kurir,
		status,
		total_harga
		FROM "order" 
	`
	rows, err := r.conn.QueryRows(ctx, query)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	order := []entity.Order{}

	for rows.Next() {
		var order_item entity.Order
		err = rows.Scan(
			&order_item.ID,
			&order_item.JumlahOrder,
			&order_item.Id_Pangkalan,
			&order_item.Id_Kurir,
			&order_item.Status,
			&order_item.Total_harga,
		)
		if err != nil {
			return nil, err
		}

		order = append(order, order_item)
	}

	return order, nil

}
func (r *orderImplementation) UpsertOrder(ctx context.Context, param entity.Order) (uint64, error) {

	q := `INSERT INTO "order" (jumlah_order, id_pangkalan, id_kurir, status, total_harga) 
			VALUES ($1,$2,$3,$4,$5) `
			

	// See https://en.wikipedia.org/wiki/Isolation_(database_systems)#Isolation_levels.
	tx, e := r.conn.BeginTx(ctx, &sql.TxOptions{
		Isolation: sql.LevelRepeatableRead,
	})

	if e != nil {
		return 0, e
	}

	values := []interface{}{
		param.JumlahOrder,
		param.Id_Pangkalan,
		param.Id_Kurir,
		param.Status,
		param.Total_harga,
		
	}
	
	result, e := tx.Exec(q, values...)

	if e != nil {
		tx.Rollback()
		return 0, e
	}

	tx.Commit()

	id, _ := result.LastInsertId()

	return uint64(id), nil
}
func (r orderImplementation) GetById(ctx context.Context, id int64) (entity.Order, error) {
	query := `
	SELECT  
	id,
	jumlah_order,
	id_pangkalan,
	id_kurir,
	status,
	total_harga
	FROM "order"
		where id = $1
	`
	var order_item entity.Order
	err := r.conn.QueryRow(ctx, query, id).Scan(
		&order_item.ID,
			&order_item.JumlahOrder,
			&order_item.Id_Pangkalan,
			&order_item.Id_Kurir,
			&order_item.Status,
			&order_item.Total_harga,
	)

	if err != nil {
		return entity.Order{}, err
	}

	return order_item, nil

}
func NewOrder(conn postgres.Adapter) Order {
	return &orderImplementation{conn: conn}
}
