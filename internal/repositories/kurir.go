// Package repositories
package repositories

import (
	"context"
	"database/sql"

	"gitlab.privy.id/go_graphql/internal/entity"
	"gitlab.privy.id/go_graphql/pkg/postgres"
)

type Kurir interface {
	Get(context.Context) ([]entity.Kurir, error)
	UpsertKurir(context.Context, entity.Kurir) (uint64, error)
}

type kurirImplementation struct {
	conn postgres.Adapter
}

func (r kurirImplementation) Get(ctx context.Context) ([]entity.Kurir, error) {
	query := `
		SELECT id,nama,id_area,user_id FROM kurir 
	`
	rows, err := r.conn.QueryRows(ctx, query)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	kurir := []entity.Kurir{}

	for rows.Next() {
		var kurir_item entity.Kurir
		err = rows.Scan(
			&kurir_item.ID,
			&kurir_item.Nama,
			&kurir_item.Id_Area,
			&kurir_item.User_ID,
		)
		if err != nil {
			return nil, err
		}

		kurir = append(kurir, kurir_item)
	}

	return kurir, nil

}
func (r *kurirImplementation) UpsertKurir(ctx context.Context, param entity.Kurir) (uint64, error) {

	q := `INSERT INTO kurir (nama, id_area, user_id) 
			VALUES ($1,$2,$3) `

	// See https://en.wikipedia.org/wiki/Isolation_(database_systems)#Isolation_levels.
	tx, e := r.conn.BeginTx(ctx, &sql.TxOptions{
		Isolation: sql.LevelRepeatableRead,
	})

	if e != nil {
		return 0, e
	}

	values := []interface{}{
		param.Nama,
		param.Id_Area,
		param.User_ID,
		
	}
	
	result, e := tx.Exec(q, values...)

	if e != nil {
		tx.Rollback()
		return 0, e
	}

	tx.Commit()

	id, _ := result.LastInsertId()

	return uint64(id), nil
}
func NewKurir(conn postgres.Adapter) Kurir {
	return &kurirImplementation{conn: conn}
}
