// Package repositories
package repositories

import (
	"context"
	"database/sql"

	"gitlab.privy.id/go_graphql/internal/entity"
	"gitlab.privy.id/go_graphql/pkg/postgres"
)

type Pangkalan interface {
	Get(context.Context) ([]entity.Pangkalan, error)
	UpsertPangkalan(context.Context, entity.Pangkalan) (uint64, error)
	GetById(ctx context.Context, id int64) (entity.Pangkalan, error)
}

type pangkalanImplementation struct {
	conn postgres.Adapter
}

func (r pangkalanImplementation) Get(ctx context.Context) ([]entity.Pangkalan, error) {
	query := `
		SELECT  
		id,
		nama_pangkalan,
		nama_pemilik,
		quota,
		alamat,
		area_id,
		user_id,
		email  FROM pangkalan
	`
	rows, err := r.conn.QueryRows(ctx, query)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	pangkalan := []entity.Pangkalan{}

	for rows.Next() {
		var pangkalan_item entity.Pangkalan
		err = rows.Scan(
			&pangkalan_item.ID,
			&pangkalan_item.NamaPangkalan,
			&pangkalan_item.NamaPemilik,
			&pangkalan_item.Quota,
			&pangkalan_item.Alamat,
			&pangkalan_item.AreaID,
			&pangkalan_item.UserID,
			&pangkalan_item.Email,
		)
		if err != nil {
			return nil, err
		}

		pangkalan = append(pangkalan, pangkalan_item)
	}

	return pangkalan, nil

}
func (r *pangkalanImplementation) UpsertPangkalan(ctx context.Context, param entity.Pangkalan) (uint64, error) {

	q := `INSERT INTO pangkalan (nama_pangkalan, nama_pemilik, email, alamat, area_id, quota, user_id) 
			VALUES ($1,$2,$3,$4,$5,$6,$7) `

	// See https://en.wikipedia.org/wiki/Isolation_(database_systems)#Isolation_levels.
	tx, e := r.conn.BeginTx(ctx, &sql.TxOptions{
		Isolation: sql.LevelRepeatableRead,
	})

	if e != nil {
		return 0, e
	}

	values := []interface{}{
		param.NamaPangkalan,
		param.NamaPemilik,
		param.Email,
		param.Alamat,
		param.AreaID,
		param.Quota,
		param.UserID,
	}

	result, e := tx.Exec(q, values...)

	if e != nil {
		tx.Rollback()
		return 0, e
	}

	tx.Commit()

	id, _ := result.LastInsertId()

	return uint64(id), nil

}
func (r pangkalanImplementation) GetById(ctx context.Context, id int64) (entity.Pangkalan, error) {
	query := `
		SELECT  
		id,
		nama_pangkalan,
		nama_pemilik,
		quota,
		alamat,
		area_id,
		user_id,
		email  FROM pangkalan
		where id = $1
	`
	var pangkalan_item entity.Pangkalan
	err := r.conn.QueryRow(ctx, query, id).Scan(
		&pangkalan_item.ID,
		&pangkalan_item.NamaPangkalan,
		&pangkalan_item.NamaPemilik,
		&pangkalan_item.Quota,
		&pangkalan_item.Alamat,
		&pangkalan_item.AreaID,
		&pangkalan_item.UserID,
		&pangkalan_item.Email,
	)

	if err != nil {
		return entity.Pangkalan{}, err
	}

	return pangkalan_item, nil

}
func NewPangkalan(conn postgres.Adapter) Pangkalan {
	return &pangkalanImplementation{conn: conn}
}

