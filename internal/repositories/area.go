// Package repositories
package repositories

import (
	"context"
	"database/sql"

	"gitlab.privy.id/go_graphql/internal/entity"
	"gitlab.privy.id/go_graphql/pkg/postgres"
)

type Area interface {
	Get(context.Context) ([]entity.Area, error)
	UpsertArea(context.Context, entity.Area) (uint64, error)
}

type areaImplementation struct {
	conn postgres.Adapter
}

func (r areaImplementation) Get(ctx context.Context) ([]entity.Area, error) {
	query := `
		SELECT  
		id,
		nama_area
		FROM "area" 
	`
	rows, err := r.conn.QueryRows(ctx, query)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	area := []entity.Area{}

	for rows.Next() {
		var area_item entity.Area
		err = rows.Scan(
			&area_item.ID,
			&area_item.NamaArea,
		)
		if err != nil {
			return nil, err
		}

		area = append(area, area_item)
	}

	return area, nil

}
func (r *areaImplementation) UpsertArea(ctx context.Context, param entity.Area) (uint64, error) {

	q := `INSERT INTO area (nama_area) 
			VALUES ($1) `

	// See https://en.wikipedia.org/wiki/Isolation_(database_systems)#Isolation_levels.
	tx, e := r.conn.BeginTx(ctx, &sql.TxOptions{
		Isolation: sql.LevelRepeatableRead,
	})

	if e != nil {
		return 0, e
	}

	values := []interface{}{
		param.NamaArea,
		
	}
	
	result, e := tx.Exec(q, values...)

	if e != nil {
		tx.Rollback()
		return 0, e
	}

	tx.Commit()

	id, _ := result.LastInsertId()

	return uint64(id), nil
}
func NewArea(conn postgres.Adapter) Area {
	return &areaImplementation{conn: conn}
}
