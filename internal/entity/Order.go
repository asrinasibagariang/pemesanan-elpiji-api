// Package entity
package entity

//go:generate easytags $GOFILE db,json

type Order struct {
	ID           	 uint64 `db:"id" json:"id"`
	JumlahOrder 	string `db:"jumlah_order" json:"jumlah_order"`
	Id_Pangkalan  	string `db:"id_pangkalan" json:"id_pangkalan"`
	Id_Kurir        string `db:"id_kurir" json:"id_kurir"`
	Status       	string `db:"status" json:"status"`
	Total_harga        string `db:"total_harga" json:"total_harga"`
}

	