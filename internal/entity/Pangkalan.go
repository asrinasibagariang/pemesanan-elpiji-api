// Package entity
package entity

//go:generate easytags $GOFILE db,json

type Pangkalan struct {
	ID            uint64 `db:"id" json:"id"`
	NamaPangkalan string `db:"nama_pangkalan" json:"nama_pangkalan"`
	NamaPemilik   string `db:"nama_pemilik" json:"nama_pemilik"`
	Quota         string `db:"quota" json:"quota"`
	Email         string `db:"email" json:"email"`
	Alamat        string `db:"alamat" json:"alamat"`
	AreaID        string `db:"area_id" json:"area_id"`
	UserID        string `db:"user_id" json:"user_id"`
}
