// Package pangkalan
package ucase

import (
	"github.com/thedevsaddam/govalidator"

	"gitlab.privy.id/go_graphql/internal/appctx"
	"gitlab.privy.id/go_graphql/internal/consts"
	"gitlab.privy.id/go_graphql/internal/entity"
	"gitlab.privy.id/go_graphql/internal/presentations"
	"gitlab.privy.id/go_graphql/internal/repositories"
	"gitlab.privy.id/go_graphql/internal/ucase/contract"
	"gitlab.privy.id/go_graphql/pkg/logger"
	"gitlab.privy.id/go_graphql/pkg/util"
)

type orderCreate struct {
	repo repositories.Order
}

// NewPartnerCreate initialize partner cerator
func NewOrderCreate(repo repositories.Order) contract.UseCase {
	return &orderCreate{repo: repo}
}

// Serve partner list data
func (u *orderCreate) Serve(data *appctx.Data) appctx.Response {

	req := presentations.OrderCreate{}

	err := data.Cast(&req)

	if err != nil {
		logger.Error(logger.MessageFormat("[order-create] parsing body request error: %v", err))
		return *appctx.NewResponse().WithCode(consts.CodeBadRequest).WithError(err.Error())
	}

	fl := []logger.Field{
		logger.Any("request", req),
	}

	rules := govalidator.MapData{
		"jumlah_order": []string{"required"},
	}

	opts := govalidator.Options{
		Data:  &req,  // request object
		Rules: rules, // rules map
	}

	v := govalidator.New(opts)
	ev := v.ValidateStruct()

	if len(ev) != 0 {
		logger.Warn(
			logger.MessageFormat("[order-create] validate request param err: %s", util.DumpToString(ev)),
			fl...)

		return *appctx.NewResponse().WithCode(consts.CodeBadRequest).WithError(err.Error())
	}

	_, err = u.repo.UpsertOrder(data.Request.Context(), entity.Order{
		JumlahOrder:   req.JumlahOrder,
		Id_Pangkalan:   req.Id_Pangkalan,
		Id_Kurir:      req.Id_Kurir,
		Status:    req.Status,
		Total_harga: req.Total_harga,
		
	})
	
	if err != nil {
		logger.Error(logger.MessageFormat("[order-create] %v", err))

		return *appctx.NewResponse().WithMessage(err).WithCode(consts.CodeInternalServerError)
	}

	return *appctx.NewResponse().WithMessage("data saved").WithCode(consts.CodeSuccess)

}