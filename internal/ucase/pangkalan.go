package ucase

import (
	"net/http"

	"gitlab.privy.id/go_graphql/internal/appctx"
	"gitlab.privy.id/go_graphql/internal/repositories"
)

type getPangkalan struct {
	pangkalanRepository repositories.Pangkalan
}

func (gp getPangkalan) Serve(data *appctx.Data) appctx.Response {
	pangkalan, err := gp.pangkalanRepository.Get(data.Request.Context())
	if err != nil {

	}
	return *appctx.NewResponse().
		WithCode(http.StatusOK).
		WithMessage("ok").
		WithData(pangkalan)
}

func NewGetPangkalan(pangkalanRepository repositories.Pangkalan) *getPangkalan {
	return &getPangkalan{
		pangkalanRepository: pangkalanRepository,
	}
}
