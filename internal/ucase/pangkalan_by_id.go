package ucase

import (
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.privy.id/go_graphql/internal/appctx"
	"gitlab.privy.id/go_graphql/internal/repositories"
)

type getPangkalanById struct {
	pangkalanRepository repositories.Pangkalan
}

func (gp getPangkalanById) Serve(data *appctx.Data) appctx.Response {
	params := mux.Vars(data.Request)
	rawID := params["id"]
	id, err := strconv.ParseInt(rawID, 10, 64)

	if err != nil {
		return *appctx.NewResponse().
			WithCode(http.StatusInternalServerError).
			WithMessage("error parsing id")

	}
	pangkalan, err := gp.pangkalanRepository.GetById(data.Request.Context(), id)
	if err != nil {
		return *appctx.NewResponse().
			WithCode(http.StatusInternalServerError).
			WithMessage("error")

	}
	return *appctx.NewResponse().
		WithCode(http.StatusOK).
		WithMessage("ok").
		WithData(pangkalan)
}

func NewGetPangkalanById(pangkalanRepository repositories.Pangkalan) *getPangkalanById {
	return &getPangkalanById{
		pangkalanRepository: pangkalanRepository,
	}
}