package ucase

import (
	"net/http"

	"gitlab.privy.id/go_graphql/internal/appctx"
	"gitlab.privy.id/go_graphql/internal/repositories"
)

type getOrder struct {
	orderRepository repositories.Order
}

func (gp getOrder) Serve(data *appctx.Data) appctx.Response {
	order, err := gp.orderRepository.Get(data.Request.Context())
	if err != nil {

	}
	return *appctx.NewResponse().
		WithCode(http.StatusOK).
		WithMessage("ok").
		WithData(order)
}

func NewGetOrder(orderRepository repositories.Order) *getOrder {
	return &getOrder{
		orderRepository: orderRepository,
	}
}
