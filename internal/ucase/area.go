package ucase

import (
	"net/http"

	"gitlab.privy.id/go_graphql/internal/appctx"
	"gitlab.privy.id/go_graphql/internal/repositories"
)

type getArea struct {
	areaRepository repositories.Area
}

func (gp getArea) Serve(data *appctx.Data) appctx.Response {
	area, err := gp.areaRepository.Get(data.Request.Context())
	if err != nil {

	}
	return *appctx.NewResponse().
		WithCode(http.StatusOK).
		WithMessage("ok").
		WithData(area)
}

func NewGetArea(areaRepository repositories.Area) *getArea {
	return &getArea{
		areaRepository: areaRepository,
	}
}
