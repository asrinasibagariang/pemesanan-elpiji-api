package ucase

import (
	"net/http"

	"gitlab.privy.id/go_graphql/internal/appctx"
	"gitlab.privy.id/go_graphql/internal/repositories"
)

type getKurir struct {
	kurirRepository repositories.Kurir
}

func (gp getKurir) Serve(data *appctx.Data) appctx.Response {
	kurir, err := gp.kurirRepository.Get(data.Request.Context())
	if err != nil {

	}
	return *appctx.NewResponse().
		WithCode(http.StatusOK).
		WithMessage("ok").
		WithData(kurir)
}

func NewGetKurir(kurirRepository repositories.Kurir) *getKurir {
	return &getKurir{
		kurirRepository: kurirRepository,
	}
}
