// Package kurir
package ucase

import (
	"github.com/thedevsaddam/govalidator"

	"gitlab.privy.id/go_graphql/internal/appctx"
	"gitlab.privy.id/go_graphql/internal/consts"
	"gitlab.privy.id/go_graphql/internal/entity"
	"gitlab.privy.id/go_graphql/internal/presentations"
	"gitlab.privy.id/go_graphql/internal/repositories"
	"gitlab.privy.id/go_graphql/internal/ucase/contract"
	"gitlab.privy.id/go_graphql/pkg/logger"
	"gitlab.privy.id/go_graphql/pkg/util"
)

type kurirCreate struct {
	repo repositories.Kurir
}

// NewPartnerCreate initialize partner cerator
func NewKurirCreate(repo repositories.Kurir) contract.UseCase {
	return &kurirCreate{repo: repo}
}

// Serve partner list data
func (u *kurirCreate) Serve(data *appctx.Data) appctx.Response {

	req := presentations.KurirCreate{}

	err := data.Cast(&req)

	if err != nil {
		logger.Error(logger.MessageFormat("[kurir-create] parsing body request error: %v", err))
		return *appctx.NewResponse().WithCode(consts.CodeBadRequest).WithError(err.Error())
	}

	fl := []logger.Field{
		logger.Any("request", req),
	}

	rules := govalidator.MapData{
		"nama": []string{"required", "between:3,50"},
	}

	opts := govalidator.Options{
		Data:  &req,  // request object
		Rules: rules, // rules map
	}

	v := govalidator.New(opts)
	ev := v.ValidateStruct()

	if len(ev) != 0 {
		logger.Warn(
			logger.MessageFormat("[kurir-create] validate request param err: %s", util.DumpToString(ev)),
			fl...)

		return *appctx.NewResponse().WithCode(consts.CodeBadRequest).WithError(err.Error())
	}

	_, err = u.repo.UpsertKurir(data.Request.Context(), entity.Kurir{
		Nama:   req.Nama,
		Id_Area:         req.Id_Area,
		User_ID:         req.User_ID,
		
	})



	if err != nil {
		logger.Error(logger.MessageFormat("[kurir-create] %v", err))

		return *appctx.NewResponse().WithMessage(err).WithCode(consts.CodeInternalServerError)
	}

	return *appctx.NewResponse().WithMessage("data saved").WithCode(consts.CodeSuccess)

}