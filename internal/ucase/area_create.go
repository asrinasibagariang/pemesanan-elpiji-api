// Package area
package ucase

import (
	"github.com/thedevsaddam/govalidator"

	"gitlab.privy.id/go_graphql/internal/appctx"
	"gitlab.privy.id/go_graphql/internal/consts"
	"gitlab.privy.id/go_graphql/internal/entity"
	"gitlab.privy.id/go_graphql/internal/presentations"
	"gitlab.privy.id/go_graphql/internal/repositories"
	"gitlab.privy.id/go_graphql/internal/ucase/contract"
	"gitlab.privy.id/go_graphql/pkg/logger"
	"gitlab.privy.id/go_graphql/pkg/util"
)

type areaCreate struct {
	repo repositories.Area
}

// NewPartnerCreate initialize partner cerator
func NewAreaCreate(repo repositories.Area) contract.UseCase {
	return &areaCreate{repo: repo}
}

// Serve partner list data
func (u *areaCreate) Serve(data *appctx.Data) appctx.Response {

	req := presentations.AreaCreate{}

	err := data.Cast(&req)

	if err != nil {
		logger.Error(logger.MessageFormat("[area-create] parsing body request error: %v", err))
		return *appctx.NewResponse().WithCode(consts.CodeBadRequest).WithError(err.Error())
	}

	fl := []logger.Field{
		logger.Any("request", req),
	}

	rules := govalidator.MapData{
		"nama_area": []string{"required", "between:3,50"},
	}

	opts := govalidator.Options{
		Data:  &req,  // request object
		Rules: rules, // rules map
	}

	v := govalidator.New(opts)
	ev := v.ValidateStruct()

	if len(ev) != 0 {
		logger.Warn(
			logger.MessageFormat("[area-create] validate request param err: %s", util.DumpToString(ev)),
			fl...)

		return *appctx.NewResponse().WithCode(consts.CodeBadRequest).WithError(err.Error())
	}

	_, err = u.repo.UpsertArea(data.Request.Context(), entity.Area{
		NamaArea:  req.NamaArea ,
		
		
	})



	if err != nil {
		logger.Error(logger.MessageFormat("[area-create] %v", err))

		return *appctx.NewResponse().WithMessage(err).WithCode(consts.CodeInternalServerError)
	}

	return *appctx.NewResponse().WithMessage("data saved").WithCode(consts.CodeSuccess)

}