package ucase

import (
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.privy.id/go_graphql/internal/appctx"
	"gitlab.privy.id/go_graphql/internal/repositories"
)

type getOrderById struct {
	orderRepository repositories.Order
}

func (gp getOrderById) Serve(data *appctx.Data) appctx.Response {
	params := mux.Vars(data.Request)
	rawID := params["id"]
	id, err := strconv.ParseInt(rawID, 10, 64)

	if err != nil {
		return *appctx.NewResponse().
			WithCode(http.StatusInternalServerError).
			WithMessage("error parsing id")

	}
	order, err := gp.orderRepository.GetById(data.Request.Context(), id)
	if err != nil {
		return *appctx.NewResponse().
			WithCode(http.StatusInternalServerError).
			WithMessage("error")

	}
	return *appctx.NewResponse().
		WithCode(http.StatusOK).
		WithMessage("ok").
		WithData(order)
}

func NewGetOrderById(orderRepository repositories.Order) *getOrderById {
	return &getOrderById{
		orderRepository: orderRepository,
	}
}