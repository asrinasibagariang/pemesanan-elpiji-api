// Package pangkalan
package ucase

import (
	"github.com/thedevsaddam/govalidator"

	"gitlab.privy.id/go_graphql/internal/appctx"
	"gitlab.privy.id/go_graphql/internal/consts"
	"gitlab.privy.id/go_graphql/internal/entity"
	"gitlab.privy.id/go_graphql/internal/presentations"
	"gitlab.privy.id/go_graphql/internal/repositories"
	"gitlab.privy.id/go_graphql/internal/ucase/contract"
	"gitlab.privy.id/go_graphql/pkg/logger"
	"gitlab.privy.id/go_graphql/pkg/util"
)

type pangkalanCreate struct {
	repo repositories.Pangkalan
}

// NewPartnerCreate initialize partner cerator
func NewPangkalanCreate(repo repositories.Pangkalan) contract.UseCase {
	return &pangkalanCreate{repo: repo}
}

// Serve partner list data
func (u *pangkalanCreate) Serve(data *appctx.Data) appctx.Response {

	req := presentations.PangkalanCreate{}

	err := data.Cast(&req)

	if err != nil {
		logger.Error(logger.MessageFormat("[pangkalan-create] parsing body request error: %v", err))
		return *appctx.NewResponse().WithCode(consts.CodeBadRequest).WithError(err.Error())
	}

	fl := []logger.Field{
		logger.Any("request", req),
	}

	rules := govalidator.MapData{
		"nama_pangkalan": []string{"required", "between:3,50"},
	}

	opts := govalidator.Options{
		Data:  &req,  // request object
		Rules: rules, // rules map
	}

	v := govalidator.New(opts)
	ev := v.ValidateStruct()

	if len(ev) != 0 {
		logger.Warn(
			logger.MessageFormat("[pangkalan-create] validate request param err: %s", util.DumpToString(ev)),
			fl...)

		return *appctx.NewResponse().WithCode(consts.CodeBadRequest).WithError(err.Error())
	}

	_, err = u.repo.UpsertPangkalan(data.Request.Context(), entity.Pangkalan{
		NamaPangkalan: req.NamaPangkalan,
		NamaPemilik:   req.NamaPemilik,
		Quota:         req.Quota,
		Email:         req.Email,
		Alamat:        req.Alamat,
		AreaID:        req.AreaID,
		UserID:        req.UserID,
	})

	if err != nil {
		logger.Error(logger.MessageFormat("[pangkalan-create] %v", err))

		return *appctx.NewResponse().WithMessage(err).WithCode(consts.CodeInternalServerError)
	}

	return *appctx.NewResponse().WithMessage("data saved").WithCode(consts.CodeSuccess)

}