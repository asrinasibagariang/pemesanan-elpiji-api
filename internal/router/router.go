// Package router
package router

import (
	"context"
	"encoding/json"
	"net/http"
	"runtime/debug"

	"gitlab.privy.id/go_graphql/internal/appctx"
	"gitlab.privy.id/go_graphql/internal/bootstrap"
	"gitlab.privy.id/go_graphql/internal/consts"
	"gitlab.privy.id/go_graphql/internal/handler"
	"gitlab.privy.id/go_graphql/internal/middleware"
	"gitlab.privy.id/go_graphql/internal/repositories"
	"gitlab.privy.id/go_graphql/internal/ucase"
	"gitlab.privy.id/go_graphql/pkg/logger"
	"gitlab.privy.id/go_graphql/pkg/msgx"
	"gitlab.privy.id/go_graphql/pkg/routerkit"

	//"gitlab.privy.id/go_graphql/pkg/mariadb"
	//"gitlab.privy.id/go_graphql/internal/repositories"
	//"gitlab.privy.id/go_graphql/internal/ucase/example"

	ucaseContract "gitlab.privy.id/go_graphql/internal/ucase/contract"
)

type router struct {
	config *appctx.Config
	router *routerkit.Router
}

// NewRouter initialize new router wil return Router Interface
func NewRouter(cfg *appctx.Config) Router {
	bootstrap.RegistryMessage()
	bootstrap.RegistryLogger(cfg)

	return &router{
		config: cfg,
		router: routerkit.NewRouter(routerkit.WithServiceName(cfg.App.AppName)),
	}
}

func (rtr *router) handle(hfn httpHandlerFunc, svc ucaseContract.UseCase, mdws ...middleware.MiddlewareFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		lang := r.Header.Get(consts.HeaderLanguageKey)
		if !msgx.HaveLang(consts.RespOK, lang) {
			lang = rtr.config.App.DefaultLang
			r.Header.Set(consts.HeaderLanguageKey, lang)
		}

		defer func() {
			err := recover()
			if err != nil {
				w.Header().Set(consts.HeaderContentTypeKey, consts.HeaderContentTypeJSON)
				w.WriteHeader(http.StatusInternalServerError)
				res := appctx.Response{
					Code: consts.CodeInternalServerError,
				}

				res.WithLang(lang)
				logger.Error(logger.MessageFormat("error %v", string(debug.Stack())))
				json.NewEncoder(w).Encode(res.Byte())

				return
			}
		}()

		ctx := context.WithValue(r.Context(), "access", map[string]interface{}{
			"path":      r.URL.Path,
			"remote_ip": r.RemoteAddr,
			"method":    r.Method,
		})

		req := r.WithContext(ctx)

		// validate middleware
		if !middleware.FilterFunc(w, req, rtr.config, mdws) {
			return
		}

		resp := hfn(req, svc, rtr.config)
		resp.WithLang(lang)
		rtr.response(w, resp)
	}
}

// response prints as a json and formatted string for DGP legacy
func (rtr *router) response(w http.ResponseWriter, resp appctx.Response) {
	w.Header().Set(consts.HeaderContentTypeKey, consts.HeaderContentTypeJSON)
	resp.Generate()
	w.WriteHeader(resp.Code)
	w.Write(resp.Byte())
	return
}

// postgresql
const (
	host     = "db.bzdphmimoxrnjpjuajbt.supabase.co"
	port     = 5432
	user     = "postgres"
	password = "oUDUKxQ2TvMHxywf"
	dbname   = "postgres"
)

// Route preparing http router and will return mux router object
func (rtr *router) Route() *routerkit.Router {
	// fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", rtr.config.WriteDB, port, user, password, dbname)

	root := rtr.router.PathPrefix("/").Subrouter()
	//in := root.PathPrefix("/in/").Subrouter()
	liveness := root.PathPrefix("/").Subrouter()
	//inV1 := in.PathPrefix("/v1/").Subrouter()

	// open tracer setup
	bootstrap.RegistryOpenTracing(rtr.config)

	//db := bootstrap.RegistryMariaMasterSlave(rtr.config.WriteDB, rtr.config.ReadDB, rtr.config.App.Timezone))
	//db := bootstrap.RegistryMariaDB(rtr.config.WriteDB, rtr.config.App.Timezone)
	db := bootstrap.RegistryPostgreSQLMasterSlave(rtr.config.WriteDB, rtr.config.ReadDB, rtr.config.App.Timezone)
	pangkalanRepository := repositories.NewPangkalan(db)
	getPangkalan := ucase.NewGetPangkalan(pangkalanRepository)

	root.Handle("/pangkalan", rtr.handle(
		handler.HttpRequest,
		getPangkalan,
	)).Methods(http.MethodGet)
	createPangkalan := ucase.NewPangkalanCreate(pangkalanRepository)

	root.Handle("/pangkalan", rtr.handle(
		handler.HttpRequest,
		createPangkalan,
	)).Methods(http.MethodPost)

	getPangkalanById := ucase.NewGetPangkalanById(pangkalanRepository)

	root.Handle("/pangkalan/{id}", rtr.handle(
		handler.HttpRequest,
		getPangkalanById,
	)).Methods(http.MethodGet)


	orderRepository := repositories.NewOrder(db)
	getOrder := ucase.NewGetOrder(orderRepository)

	root.Handle("/order", rtr.handle(
		handler.HttpRequest,
		getOrder,
	)).Methods(http.MethodGet)

	createOrder := ucase.NewOrderCreate(orderRepository)

	root.Handle("/order", rtr.handle(
		handler.HttpRequest,
		createOrder,
	)).Methods(http.MethodPost)

	getOrderById := ucase.NewGetOrderById(orderRepository)

	root.Handle("/order/{id}", rtr.handle(
		handler.HttpRequest,
		getOrderById,
	)).Methods(http.MethodGet)

	kurirRepository := repositories.NewKurir(db)
	getKurir := ucase.NewGetKurir(kurirRepository)

	root.Handle("/kurir", rtr.handle(
		handler.HttpRequest,
		getKurir,
	)).Methods(http.MethodGet)

	createKurir := ucase.NewKurirCreate(kurirRepository)

	root.Handle("/kurir", rtr.handle(
		handler.HttpRequest,
		createKurir,
	)).Methods(http.MethodPost)

	areaRepository := repositories.NewArea(db)
	getArea := ucase.NewGetArea(areaRepository)

	root.Handle("/area", rtr.handle(
		handler.HttpRequest,
		getArea,
	)).Methods(http.MethodGet)
	createArea := ucase.NewAreaCreate(areaRepository)

	root.Handle("/area", rtr.handle(
		handler.HttpRequest,
		createArea,
	)).Methods(http.MethodPost)
	// use case
	healthy := ucase.NewHealthCheck()

	// healthy
	liveness.HandleFunc("/liveness", rtr.handle(
		handler.HttpRequest,
		healthy,
	)).Methods(http.MethodGet)

	// result :=

	// db := bootstrap.RegistryPostgreSQLMasterSlave(rtr.config.ReadDB, rtr.config.WriteDB, rtr.config.App.Timezone)

	// this is use case for example purpose, please delete
	//repoExample := repositories.NewExample(db)
	//el := example.NewExampleList(repoExample)
	//ec := example.NewPartnerCreate(repoExample
	//ed := example.NewExampleDelete(repoExample)

	// TODO: create your route here

	// this route for example rest, please delete
	// example list
	//inV1.HandleFunc("/example", rtr.handle(
	//    handler.HttpRequest,
	//    el,
	//)).Methods(http.MethodGet)

	//inV1.HandleFunc("/example", rtr.handle(
	//    handler.HttpRequest,
	//    ec,
	//)).Methods(http.MethodPost)

	//inV1.HandleFunc("/example/{id:[0-9]+}", rtr.handle(
	//    handler.HttpRequest,
	//    ed,
	//)).Methods(http.MethodDelete)

	return rtr.router

}
