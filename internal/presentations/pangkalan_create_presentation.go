// Package presentations
package presentations

type PangkalanCreate struct {
	NamaPangkalan string `json:"nama_pangkalan"`
	NamaPemilik   string `json:"nama_pemilik"`
	Quota         string `json:"quota"`
	Email         string `json:"email"`
	Alamat        string `json:"alamat"`
	AreaID        string `json:"area_id"`
	UserID        string `json:"user_id"` 
}